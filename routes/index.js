const express = require('express');
const router = express.Router();
const controller = require('../controllers/index');

/* GET home page. */
router.get('/', () => {
    res.render('index.html');
});

router.post('/getmessage', controller.getMessage);

router.post('/sendmessage', controller.sendMessage);

module.exports = router;
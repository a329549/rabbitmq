# Small aplication with RabbitMQ 🐇
This is a small web project to practice what I have learned with RabbitMQ.
## Project demo 👀
![](demo.png)
## Required software to run the project 📚
| Software | Download |
| ------ | ------ |
| Docker | https://www.docker.com/ |
| NodeJS | https://nodejs.org/ |
## Installation instructions 🧱
Commands to install and run the application locally.
First step: clone the repository and install the dependencies:
```
$ git clone https://gitlab.com/a329549/rabbitmq.git
$ cd ./rabbitmq
$ npm install
```
Second step: run a Docker container with the image of RabbitMQ:
```
$ docker run -dti -P --hostname=rabbit --name=rabbit -e RABBITMQ_DEFAULT_USER=edwin -e RABBITMQ_DEFAULT_PASS=abc123 rabbitmq:3-management
```
Third step: Cambiar el valor del puerto correspondiente al 5672 de nuestra maquina en el archivo .env del proyecto.

Fourth step: Execute the NodeJS APP:
```
$ npm start
```

Fifth step: Go to http://localhost:3000/ to see the chat.
## Developer 🛠️
> EDWIN ALFREDO PINEDO CHACON ( a329549@uach.mx )
## Licencia ✔️
---
#### MIT
##### ( Software gratis, matanga... 🏃 )

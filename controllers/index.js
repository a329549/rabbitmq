const amqp = require('amqplib/callback_api');

const sendMessage = (req, res, next) => {

    amqp.connect(process.env.RABBITMQ_CONNECTION, (err, con) => {

        if (err) throw err;

        con.createChannel((err, channel) => {

            if (err) throw err;

            const messageObj = { message: req.body.message, user: req.body.sourceUser };

            channel.assertQueue(req.body.targetUser, { durable: false });
            channel.sendToQueue(req.body.targetUser, Buffer.from(JSON.stringify(messageObj)));
            console.log("Mensaje enviado");
            res.status(200).json({
                message: 'Mensaje enviado.',
                obj: messageObj
            });
        });
        setTimeout(() => { con.close(); }, 500);
    });
}

const getMessage = (req, res, next) => {

    amqp.connect(process.env.RABBITMQ_CONNECTION, (err, con) => {

        if (err) throw err;

        con.createChannel((err, channel) => {

            if (err) throw err;

            channel.assertQueue(req.body.queue, {
                durable: false
            });

            channel.consume(req.body.queue, (message) => {
                const content = JSON.parse(message.content.toString())
                console.log(content);
                res.status(200).json({
                    message: content
                });
            }, { noAck: true });
        });

        setTimeout(() => { con.close(); }, 500);
    });
};

module.exports = {
    sendMessage,
    getMessage
}